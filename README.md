# JWT Firebase Realtime Database Typescript ExpressJS REST API

Register, Login, Get Profile

### Folder Structure

```
src - Root directory
├── config - all file configuration
├── controllers - Get all requests from route and pass to service
│── middlewares - to validate before passing to the controller
└── repositories - handle Query Logic
└── routes - Routes http
└── services - Layer that will handles the business logic in each domain.
```

## Getting started
### Requirements

- NodeJS v12.12.0 Or higher
### Installation

copy `.env-example` and rename to `.env` 

```console
NODE_DEV=development
APP_PORT=3000

FB_APIKEY=YOUR_API_KEY
FB_DB_URL=https://deepshit.com

JWT_SECRET_KEY=DEEPSHIT

```

Install dependency

```console
npm install

```

Build Code

```console
npm run tsc
```

Build & Watch

```console
npm run tsc
```

Run Application

```console
npm run dev
```

### Quick Installation Using Docker Compose

```console
docker-compose up --build
```

### Test Endpoint with Postman

- Import `collection` from `postman/collection/JWT_FIREBASE_RTDB.postman_collection.json`
- Import `environtment` Collection from `postman/environtment/ENV_LOCAL_JWT_FIREBASE_RTDB.postman_environment.json`

Fill Environtment Postman `token` & `port`

Open collection in folder `JWT_FIREBASE_RTDB/Authentication` on your postman. 

Register Body Raw Json

```console
{
    "email":"administrator@gmail.com",
    "name": "Administrator",
    "address": "Tangerang",
    "password": "12345678",
    "confirm_password": "12345678"
}

```

Login Body Raw Json

```console
{
    "email":"administrator@gmail.com",
    "password": "12345678"
}

```

Results Login 
```
{
    "message": "seccess login",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluaXN0cmF0b3JAZ21haWwuY29tIiwiaWF0IjoxNjM5MTY3MTE2fQ.9o6gY9uiC3jk2wldZ-HfH_hx5NKG1BDePbc9z98oGzI"
}
```

copy token into environtment `Postman` 


![Postman SS](postman_ss.png)

### Thank You 

GinanjarDP
[Linkeddin](https://id.linkedin.com/in/ginanjar-putranto-0416a913b)
