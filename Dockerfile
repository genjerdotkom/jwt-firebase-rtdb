FROM node:12-alpine AS builder
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run tsc
EXPOSE 3000
CMD ["npm", "run", "dev"]