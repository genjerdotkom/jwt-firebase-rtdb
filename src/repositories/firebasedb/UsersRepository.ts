import firebase from "../../config/firebase";

class UsersRepository {
    private static instance: UsersRepository;
    private database: any;
    
    constructor(){
        this.database = firebase.database().ref('/users');
    }

    public static getInstance(): UsersRepository{
        if(!UsersRepository.instance){
            UsersRepository.instance = new UsersRepository();
        }
        return UsersRepository.instance;
    }

    get = async () => {
        let usersRef = await this.database;
        return await usersRef.once('value', function(snapshot: any) { return snapshot })
    }

    store = async (data: any) => {
        let usersRef = await this.database;
        usersRef.push().set(data)
        return {
            message: "success registered"
        }
    }

    findOne = async (email: string) => {
        let snapshot = await this.get();
        let exists: boolean = false;
        let data: any = {};
        snapshot.forEach(function(childSnapshot: any){
            if(email === childSnapshot.val().email){
                exists = true;     
                data = childSnapshot.val()
            }
        })
        return { data, exists }

    }

}

export default UsersRepository