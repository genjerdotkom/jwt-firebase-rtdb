import { Request, Response } from "express";
import Services from "../services/AuthenticationService";
class AuthController{

    register = async (req: Request, res: Response): Promise<Response> => {
        try{
            const service: Services = new Services(req);
            const callback = await service.register(); 
            return res.status(callback.statusCode).send(callback.data)
        }catch(err: any){
            return res.status(500).send(err.message)
        }
    }

    login = async (req: Request, res: Response): Promise<Response> => {
        try{
            const service: Services = new Services(req);
            const callback = await service.login(); 
            return res.status(callback.statusCode).send(callback.data)
        }catch(err: any){
            return res.status(500).send(err.message)
        }
    }

    profile = (req: Request, res: Response): Response => {
        try{
            const service: Services = new Services(req);
            const callback = service.profile(); 
            return res.status(callback.statusCode).send(callback.data)
        }catch(err: any){
            return res.status(500).send(err.message)
        }
    }
   
}

export default new AuthController();