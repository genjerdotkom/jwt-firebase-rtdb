import { Request } from "express";
import Authentication from "../utils/Authentication";
import UsersRepository from "../repositories/firebasedb/UsersRepository";

class AuthenticationService {
    public body: Request['body'];
    public credentials: Request['app'];
    public params: Request['params'];
    private UsersRepository:UsersRepository;
    
    constructor(req: Request){
        this.body = req.body;
        this.params = req.params;
        this.credentials = req.app.locals.credential;
        this.UsersRepository = UsersRepository.getInstance();
    }

    register = async () => {
        try{
            const { email, password, name, address } = this.body;
            const hashedPassword: string = await Authentication.passwordHash(password);
            const isExists = await (await this.UsersRepository.findOne(email)).exists;
            if(isExists){
                return {
                    statusCode: 200,
                    data: {
                        message: "Email Exists!"
                    }
                }
            }

            const store = await this.UsersRepository.store({
                email, 
                name,
                address,
                password: hashedPassword
            });

            return {
                statusCode: 200,
                data: store
            }
        }catch(err: any){
            throw new Error(err.message)
        }
    }
    
    login = async () => {
        try{
            const { email, password } = this.body;
            const user = await this.UsersRepository.findOne(email);
            const isExists = user.exists;
            if(isExists){
                const compare = await Authentication.passwordCompare(password, user.data.password);
                if(compare){
                    const token = await Authentication.generateToken({
                        email, 
                        name: user.data.name, 
                        address: user.data.address
                    });
                    return {
                        statusCode: 200,
                        data: {
                            message: "Success login",
                            token
                        }
                    }
                }
            }
            return {
                statusCode: 401,
                data:{
                    message:"Auth failed"
                }
            };

        }catch(err: any){
            throw new Error(err.message)
        }
    }

    profile = () => {
        return {
            statusCode: 200,
            data: this.credentials
        };
    }
}

export default AuthenticationService;