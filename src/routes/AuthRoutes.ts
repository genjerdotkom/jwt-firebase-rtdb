import BaseRoutes from "./base/BaseRouter";
import registerValidator from "../middlewares/validators/RegisterValidator";
import loginValidator from "../middlewares/validators/LoginValidator";
import { auth } from "../middlewares/AuthMiddleware";
// Controller
import AuthController from "../controllers/AuthController";

class AuthRoutes extends BaseRoutes{
    public routes():void{
        this.router.post("/register", registerValidator, AuthController.register)
        this.router.post("/login", loginValidator, AuthController.login)
        this.router.get("/profile", auth, AuthController.profile);
    }
}

export default new AuthRoutes().router;