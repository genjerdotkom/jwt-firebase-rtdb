import { Request, Response, NextFunction } from "express";
import { check, validationResult } from "express-validator";

const validate = [
    check('email').isEmail(),
    check('name').isString(),
    check('address').isString(),
    check('password').isLength({min:8}),
    check('confirm_password')
        .isLength({ min: 8 })
        .custom((value, { req }) => {
            if (value !== req.body.password) {
              throw new Error('Password confirmation does not match password');
            }
            return true;
        }),
    ( req: Request, res: Response, next: NextFunction ) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(422).send({ message:{errors: errors.array() }});
        }
        return next();
    }
]

export default validate;