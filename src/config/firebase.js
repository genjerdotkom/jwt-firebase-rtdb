import { initializeApp } from "firebase";
require("dotenv").config()
const firebaseConfig = {
  apiKey: process.env.FB_APIKEY,
  databaseURL: process.env.FB_DB_URL
};
const firebase = initializeApp(firebaseConfig);
export default firebase